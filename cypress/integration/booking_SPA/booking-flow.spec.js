// should run different tests depending on environment
// await/sync ?
// + intellisence
// + test in different screen sizes

/// <reference types="cypress"/>

Cypress.config({
    "baseUrl": "https://staging-booking.jayride.com"
});

describe('Basic booking flow', () => {
    // it('should pass', () => {
    //     expect(true).to.equal(true);
    // });
    // it('should fail', () => {
    //     expect(true).to.equal(false);
    // });

    beforeEach(() => {
        cy.viewport(1024, 768); // set desctop view
    });

    it('should make a booking using default search criteria', () => {
        cy.visit('/');

        cy.contains('Compare').click();
        cy.url().should('include', '/find');

        cy.get('button[name="selectQuoteBtn"]').eq(0).click();
        cy.url().should('include', '/booking-details');

        let passengerDetails = cy.get('.jr-pax-booking-passengers-details');
        passengerDetails.get('input[name="givenName"]').type('Dima');
        passengerDetails.get('input[name="name"]').type('Tolkachov');
        passengerDetails.get('input[name="email"]').first().type('dmitriy@jayride.com');
        passengerDetails.get('input[name="flightNumber"]').type('EY455');
        passengerDetails.get('.selected-flag').click().type('Au');
        passengerDetails.get('.iti-flag.au').first().click();
        passengerDetails.get('input[name="mobileNumber"]').type('0411222333');

        let paymentDetails = cy.get('.jr-pax-booking-payment-details');
        paymentDetails.get('input[name="cardNumber"]').type('4444333322221111');
        // TODO: select expiration date
        paymentDetails.get('input[name="cvv"]').type('111');
        paymentDetails.get('input[name="cardHolder"]').type('Dima');



        cy.get('[name="confirmPaymentBtn"]').click();
        cy.url().should('include', '/booking-confirmation');

        // cy.get('button[name="update-search"]').type('').should('have.value', '');
    });
});